import React, {Component} from 'react';
import "./App.scss";

import LoginContainer from "./Containers/LoginContainer";

class App extends Component {

  state = {
    showing_login: true
  };

  show_login = (bool) => {
    this.setState({showing_login: bool})
  };

  render() {
    let login = null;
    let home_page = null;

    if (this.state.showing_login){
      login = (
        <LoginContainer show_login = {this.show_login}/>
      );
    } else {
      home_page = (
          <div className="HomePage">
            <button onClick={() => this.show_login(true)}>Log Out</button>
          </div>
      );
    }

    return (
        <div className="main">
          {login}
          {home_page}
        </div>
    );
  }
}
export default App;
