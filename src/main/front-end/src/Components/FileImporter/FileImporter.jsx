import React, {Component} from 'react';
import './FileImporter.css';

class FileImporter extends Component {

    state = {
        current_uploaded_text: null,
        current_uploaded_image: null
    };

    reset_current_uploads = () => {
        this.setState({
            current_uploaded_text: null,
            current_uploaded_image: null
        });
    };

    import_file = (event,type) => {
        const file = event.target.files[0];
        const reader = new FileReader();
        if (type === "image") {
            reader.onload = (e) => {
                const image = new Image();
                image.src = reader.result;
                console.log(reader.result);
                this.setState({current_uploaded_image: reader.result})
            };
            reader.readAsDataURL(file);

        } else if (type === "text") {
            reader.onload = (e) => {
                let fileText = e.target.result;
                console.log(fileText);
                if (fileText !== "") {
                    this.setState({current_uploaded_text: fileText});
                }
            };
            reader.readAsText(file);
        }
    };


    render = () => {
        const imported_text = this.state.current_uploaded_text;
        const imported_image = this.state.current_uploaded_image;
        let current_upload, new_upload = null;
        let upload_options = (
                <React.Fragment>
                    <input  type="file" id="text_import" className="hidden" onChange={(e) => this.import_file(e,"text")} />
                    <label htmlFor="text_import" className="text_import_label">Import Text</label>

                    <input  type="file" id="image_import" className="hidden" onChange={(e) => this.import_file(e,"image")} />
                    <label htmlFor="image_import" className="image_import_label">Import Image</label>
                </React.Fragment>
            );

        if (imported_text != null){
            const paragraphs = imported_text.split("\n").map( line => {
                return <p className="book_line">{line}</p>
            });
            current_upload = (
                <div className="book_text">
                    {paragraphs}
                </div>
            );
        } else if (imported_image != null) {
            current_upload = (
                <div className="book_image">
                    <img src={imported_image} />
                </div>
            );
        }

        if (current_upload != null) {
           upload_options = null;
           new_upload = <button className = "new_upload" onClick={ () => this.reset_current_uploads() } > Upload a different file </button>
        }

        return (
            <div className="file_importer">
                {upload_options}
                {new_upload}
                {current_upload}
            </div>
        )
    }
}

export default FileImporter;