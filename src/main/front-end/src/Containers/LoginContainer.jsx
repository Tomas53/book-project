import React, {Component} from 'react';
import Login from "../Components/Login/Login"
import Register from "../Components/Login/Register"
import RightSide from "../Components/RightSide/RightSide";


class LoginContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoginActive: true,
    };
  }

  componentDidMount() {
    //Add .right by default
    this.rightSide.classList.add("right");
  }

  changeState() {
    const { isLoginActive } = this.state;

    if (isLoginActive) {
      this.rightSide.classList.remove("right");
      this.rightSide.classList.add("left");
    } else {
      this.rightSide.classList.remove("left");
      this.rightSide.classList.add("right");
    }
    this.setState(prevState => ({ isLoginActive: !prevState.isLoginActive }));
  }


  render() {
    const { isLoginActive } = this.state;
    const current = isLoginActive ? "Register" : "Login";
    const currentActive = isLoginActive ? "login" : "register";

    return (
        <div className="App">
          <div className="login">
            <div className="container" ref={ref => (this.container = ref)}>
              {isLoginActive && (
                  <Login containerRef={ref => (this.current = ref)} show_login={this.props.show_login} />
              )}
              {!isLoginActive && (
                  <Register containerRef={ref => (this.current = ref)} />
              )}
            </div>
            <RightSide
                current={current}
                currentActive={currentActive}
                containerRef={ref => (this.rightSide = ref)}
                onClick={this.changeState.bind(this)}
            />
          </div>
        </div>
    );
  }
}
export default LoginContainer;