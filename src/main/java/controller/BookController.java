package controller;

import domain.Book;
import service.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {

    private BookService bookService = new BookService();


    @RequestMapping(value = "/books", method = RequestMethod.GET, headers = "Accept=application/json")
    public List<Book> getBooks() {
        List<Book> listOfBooks = bookService.getAllBooks();
        return listOfBooks;
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public Book getBookById(@PathVariable int id){
        return bookService.getBook(id);
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST, headers = "Accept=application/json")
    public Book addBook(@RequestBody Book book){
        return bookService.addBook(book);
    }

    @RequestMapping(value = "/books", method = RequestMethod.PUT, headers = "Accept=application/json")
    public Book updateBook(@RequestBody Book book){
        return bookService.updateBook(book);
    }

    @RequestMapping(value = "/book/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void deleteBook(@PathVariable("id") int id ) { bookService.deleteBook(id);}

}
