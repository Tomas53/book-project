package controller;

import domain.Fork;
import org.springframework.web.bind.annotation.*;
import service.ForkService;

import java.util.List;

@RestController
public class ForkController {

    private ForkService forkService = new ForkService();


    @RequestMapping(value = "/{bookId}/forks", method = RequestMethod.GET, headers = "Accept=application/json")
    public List<Fork> getForks(@PathVariable int bookId) {
        List<Fork> listOfForks = forkService.getAllForksByBookId(bookId);
        return listOfForks;
    }


    @RequestMapping(value = "/{bookId}/{forkId}", method = RequestMethod.GET, headers = "Accept=application/json")
    public Fork getForkById(@PathVariable int bookId, @PathVariable int forkId) {
        return forkService.getForkByBook(bookId, forkId);
    }

    @RequestMapping(value = "/{bookId}/forks", method = RequestMethod.POST, headers = "Accept=application/json")
    public Fork addFork(@PathVariable int bookId, @RequestBody Fork fork) {
        return forkService.addFork(bookId, fork);
    }

//    @RequestMapping(value = "/{bookId}/forks", method = RequestMethod.PUT, headers = "Accept=application/json")
//    public Fork updateFork(@PathVariable int bookId, @RequestBody Fork fork){
//        return forkService.updateFork(fork);
//    }
//
//    @RequestMapping(value = "/{bookId}/{forkId}", method = RequestMethod.DELETE, headers = "Accept=application/json")
//    public void deleteFork(@PathVariable int bookId, @PathVariable int forkId){
//        forkService.deleteFork(forkId);
//    }

}
