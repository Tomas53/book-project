package domain;

import java.util.List;

public class Book {
    private int id;
    private String author;
    private String title;
    private List<Fork>forks;
    private String text;


    public Book() {
        super();
    }

    public Book(int id, String author, String title, List<Fork> forks, String text) {
        super();
        this.id = id;
        this.author = author;
        this.title = title;
        this.forks =forks;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Fork> getForks() {
        return forks;
    }

    public void setForks(List<Fork> forks) {
        this.forks = forks;
    }
}
