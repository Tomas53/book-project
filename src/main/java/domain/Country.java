package domain;

public class Country {

	private int id;
	private String countryName;
	private long contributors;

	public Country() {
		super();
	}

	public Country(int i, String countryName, long contributors) {
		super();
		this.id = i;
		this.countryName = countryName;
		this.contributors = contributors;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public long getContributors() {
		return contributors;
	}

	public void setContributors(long contributors) {
		this.contributors = contributors;
	}

}