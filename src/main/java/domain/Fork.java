package domain;

public class Fork {

    private int forkId;
    private int bookId;
    private String author;
    private ForkStatusEnums status;
    private int upVotes;
    private int downVotes;
    private String newText;

// No touching... this for default constructor
    public Fork() {
        super();
    }
// constructor
    public Fork(int forkId, int bookId, String author, ForkStatusEnums status, int upVotes, int downVotes, String newText) {
        super();
        this.forkId = forkId;
        this.bookId = bookId;
        this.author = author;
        this.status = status;
        this.upVotes = upVotes;
        this.downVotes = downVotes;
        this.newText = newText;
    }

    public int getForkId() {
        return forkId;
    }

    public void setForkId(int forkId) {
        this.forkId = forkId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ForkStatusEnums getStatus() {
        return status;
    }

    public void setStatus(ForkStatusEnums status) {
        this.status = status;
    }

    public int getUpVotes() {
        return upVotes;
    }

    public void setUpVotes(int upVotes) {
        this.upVotes = upVotes;
    }

    public int getDownVotes() {
        return downVotes;
    }

    public void setDownVotes(int downVotes) {
        this.downVotes = downVotes;
    }

    public String getNewText() {
        return newText;
    }

    public void setNewText(String newText) {
        this.newText = newText;
    }
}

