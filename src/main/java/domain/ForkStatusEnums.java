package domain;

public enum ForkStatusEnums {
    OPEN, APPROVED, DECLINED
}
