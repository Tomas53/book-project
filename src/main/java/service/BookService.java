package service;

import domain.Book;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class BookService {


    static HashMap<Integer, Book> bookIdMap = getBookIdMap();

    public BookService() {
        super();

        if (bookIdMap == null){
            bookIdMap = new HashMap<Integer, Book>();

            bookIdMap.put(1, new Book(1,"Femi","Test Book 1", Collections.emptyList(),"Chapter 1\n Once upon a time there was a great book..."));
            bookIdMap.put(2, new Book(2,"Femi","Test Book 1", Collections.emptyList(),"Chapter 1\n Once upon a time there was a great book..."));
            bookIdMap.put(3, new Book(3,"Peter","Test Book 1", Collections.emptyList(),"Chapter 1\n Once upon a time there was a great book..."));
            bookIdMap.put(4, new Book(4,"Tomas","Test Book 1", Collections.emptyList(),"Chapter 1\n Once upon a time there was a great book..."));
            bookIdMap.put(5, new Book(5,"Andrei","Test Book 1", Collections.emptyList(),"Chapter 1\n Once upon a time there was a great book..."));


        }
    }

    public List<Book> getAllBooks(){
        List<Book> books = new ArrayList<Book>(bookIdMap.values());
       return books;
    }

    public Book getBook(int id){
        Book book = bookIdMap.get(id);
        return book;
    }

    public Book addBook(Book book){
        book.setId(getMaxId() + 1);
        bookIdMap.put(book.getId(), book);
        return book;
    }

    public Book updateBook(Book book){
        if(book.getId() <= 0){
            return null;
        }
        bookIdMap.put(book.getId(),book);
        return book;
    }

    public void deleteBook(int id){
        bookIdMap.remove(id);
    }

    public static HashMap<Integer, Book> getBookIdMap() {
        return bookIdMap;
    }


    // Kinda scuffed but in sql or mongo you can just order by id instead
    public static int getMaxId() {
        int max = 0;
        for (int id : bookIdMap.keySet()) {
            if (max <= id)
                max = id;

        }
        return max;
    }

}
