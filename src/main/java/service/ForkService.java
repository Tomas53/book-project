package service;

import domain.Fork;


import java.util.ArrayList;
import java.util.List;

public class ForkService {

    private List<Fork> forkList = getForkList();

    public ForkService() {
        super();

        if (forkList == null) {
            forkList = new ArrayList<>();
        }

    }

    public List<Fork> getAllForksByBookId(int bookId) {
        List<Fork> forks = new ArrayList<>();
        for (Fork _fork : forkList) {
            if (_fork.getBookId() == bookId) forks.add(_fork);
        }
        return forks;
    }

    public Fork getForkByBook(int bookId, int forkId) {

        List<Fork> forksByBook = getAllForksByBookId(bookId);

        for (Fork _fork : forksByBook) {
            if (_fork.getForkId() == forkId) {
                return _fork;
            }
        }
        return null;
    }

    public Fork addFork(int bookId, Fork fork) {

        fork.setBookId(bookId);
        fork.setForkId(getMaxId() + 1);
        forkList.add(fork);
        return fork;
    }

    public List<Fork> getForkList() {
        return forkList;
    }


    public int getMaxId() {
        int max = 0;
        List<Integer> idList = new ArrayList<>();
        for (Fork fork : forkList) {
            idList.add(fork.getForkId());
            for (int id : idList) {
                if (max <= id)
                    max = id;
            }

        }
        return max;
    }

}
